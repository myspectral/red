import { writable, readable } from 'svelte/store';

export interface localResult {
    UUID: string;
    img: string;
    usertitle: string;
    allergens: string[];
    imgrlist: string[];
    ingrlist: string[][];
    score: number[];
    scorecolor: string[];
    titlelist: string[];    
    matrix: string[];
    datetime: string[];
    status: boolean;
};


export interface Result {
    UUID: string;
    img: string;
    usertitle: string;
    allergens: string[];
    imgrlist: string[];
    ingrlist: string[][];
    score: number[];
    scorecolor: string[];
    titlelist: string[];    
};

export function getData() {
    const settings = localStorage.getItem('settings');
    if (settings)
        return JSON.parse(settings);
    else
        return {
            debugMode: false,
            username: '',
            inputDeviceIP: '',
            inputUploadURL: '',
            allergens: {
                "Peanuts": true,
                "Avocado": false,
                "Nuts": true,
                "Soy": true,
                // "Gluten": true,
                // "Crustaceans": false,
                // "Eggs": false,
                // "Fish": false,
                // "Lupin": false,
                // "Milk": false,
                // "Molluscs": false,
                // "Mustard": false,
                // "Sesame": false,
                // "Soybeans": false,        
                // "Tree Nuts": false,
                // "Celery": false,
                // "Sulphur Dioxide": false,    
            }

        }
}

export const results = writable([] as Result[]);
export const reslocal = writable([] as localResult[]);
export const debugMode = writable(getData().debugMode);
//export const allergens = getData().allergens;
export const allergens = writable(getData().allergens);

export function saveData(settings: any) {
    localStorage.setItem('settings', JSON.stringify(settings));
}

