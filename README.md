# Myspectral svelte app


## CHANGELOG 

### v0.4
- adding CI/CD on gitlab pages
- redesign index.html for pages deployment
- pages deployed now under: 
- http://myspectral.gitlab.io/red/
- Blocked mixed active content https://stackoverflow.com/questions/18251128/why-am-i-suddenly-getting-a-blocked-loading-mixed-active-content-issue-in-fire
- adding version number to ./public/index.html
- adding disabled property for Checkbox and checkbox debug disabled in Settings.svelte .. change if you want debug results 

### v0.3 
- Added default options.
- Switched to new esp32 server
- Changed URL for checking alive status
- Changed timeout
- Added spectrometer measurement

- These changes break all previous compatibility with ESP32 camera.
- Might require doing update on if (spectrometer) then (code for spectrometer ) else (code for ESP32 only) 

### V0.2 
- added debugTrue results 



## Installation
1. Check if you have npm installed using `npm -v` (install node if you don't)
2. Go to the root directory of this project (esp32cam)
3. Run `npm install`


## Run in development mode
Run `npm run dev` to start it on localhost:5000

## Call the URL with the following commands 

http://localhost:5000/?device-ip=esp32.local&upload-url=http://red.myspectral.com/upload

where: 

esp32.local is the mDNS record for the esp32cam and red.myspectral.com or red.myspectral.com/upload is the web page for the form processing.

**Attention!**

On Android systems, the mDNS will NOT work, so the user is required to find out the correct IP address of the device on his local network, either through router or an app like : 

https://play.google.com/store/apps/details?id=com.myprog.netscan&hl=en_US&gl=US



## Build
Run `npm run build` to build the static files for the app. The files will be located in the `public` folder.


